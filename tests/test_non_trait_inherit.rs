use inheriters::specialisations;

specialisations!(
    #[allow(clippy::upper_case_acronyms)]
    #[allow(dead_code)]
    struct ABC {
        attr1: u8,
    }

    impl ABC {
        #[allow(dead_code)]
        fn some_function(self) -> u8{
            99
        }
        #[allow(dead_code)]
        fn overridden(self) -> u8 {
            1
        }
    }

    #[allow(clippy::upper_case_acronyms)]
    #[allow(dead_code)]
    #[inherit(ABC)]
    #[derive(Copy, Clone)]
    struct CDE {
        #[allow(dead_code)]
        attr2: u8,
    }

    impl CDE {
        fn overridden(self) -> u8 {
            self.attr2
        }
    }

    #[allow(clippy::upper_case_acronyms)]
    #[allow(dead_code)]
    #[inherit(CDE)]
    struct EFG {
    }

    impl EFG {
        fn overridden(self) -> u8 {
            self.attr1
        }
    }
);

#[test]
fn test_simple_non_trait_func_inheritance() {
    let cde = CDE { attr1: 1, attr2: 2 };
    assert_eq!(cde.some_function(), 99);
    assert_eq!(cde.overridden(), 2);
}

#[test]
/// Check if `overridden` was indeed overridden 2 children down from base
/// ABC -> CDE -> EFG
fn test_deeper_non_trait_func_inheritance() {
    let cde = EFG {
        attr1: 10,
        attr2: 2,
    };
    assert_eq!(cde.overridden(), 10);
}
