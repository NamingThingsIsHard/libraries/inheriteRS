use inheriters::specialisations;

specialisations!(
    #[allow(clippy::upper_case_acronyms)]
    #[allow(dead_code)]
    struct ABC {
        attr1: u8,
    }
    #[inherit(ABC)]
    #[allow(clippy::upper_case_acronyms)]
    #[allow(dead_code)]
    struct CDE {
        attr2: u8,
    }
);

#[test]
fn test_simple_inherit() {
    let cde = CDE { attr1: 1, attr2: 2 };
    assert_eq!(cde.attr1, 1);
    assert_eq!(cde.attr2, 2);
}
