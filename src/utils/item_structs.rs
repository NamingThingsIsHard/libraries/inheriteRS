use syn::ItemStruct;

use crate::models::{BaseAttribute, InheritAttribute};
use crate::utils::predicates;

/// Retrieve the names of the base (parent) structs
pub fn extract_parent_names(item: &ItemStruct) -> Vec<String> {
    item.attrs
        .iter()
        .map(|attr| InheritAttribute::try_from((*attr).clone()))
        .filter(predicates::is_ok)
        .map(|x| x.expect("Expected an attribute").base)
        .collect()
}

/// Retrieves the `#[base]` attribute from a struct if one exists
pub fn extract_base_attr(item: &ItemStruct) -> Option<BaseAttribute> {
    let base_attrs: Vec<BaseAttribute> = item
        .attrs
        .iter()
        .map(|attr| BaseAttribute::try_from((*attr).clone()))
        .filter(predicates::is_ok)
        .map(|x| x.expect("Expected an attribute"))
        .collect();
    match base_attrs.len() {
        0 => None,
        _ => base_attrs.first().cloned(),
    }
}
