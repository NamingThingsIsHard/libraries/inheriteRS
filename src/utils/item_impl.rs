use indexmap::IndexMap;
use proc_macro2::{Ident, Span};
use quote::ToTokens;
use syn::punctuated::Punctuated;
use syn::{
    Attribute, ImplItem, ImplItemFn, Item, ItemImpl, Path, PathSegment, Token, Type, TypePath,
};

use crate::utils::predicates::clone;

/// From "impl ... for $target" get $target
pub fn get_target(item_impl: &ItemImpl) -> Option<String> {
    match item_impl.clone().self_ty.as_ref() {
        Type::Path(path) => Some(path.to_token_stream().to_string()),
        _ => None,
    }
}

/// Take multiple impl blocks and merge them into one
///
/// Functions and attributes will be deduplicated.
///
/// Functions are deduplicated by their signature, with the last function in the list winning!
/// That means, the parent implementation blocks must come first and the child last
pub fn merge_impl_blocks(target: &str, item_impls: Vec<ItemImpl>) -> Item {
    let mut existing_functions: IndexMap<String, ImplItemFn> = IndexMap::new();
    let mut existing_attrs: IndexMap<String, Attribute> = IndexMap::new();
    let mut impl_items: Vec<ImplItem> = vec![];

    for item_impl in item_impls {
        // Deduplicate functions with the last in the list overwriting all previous duplicates
        for item in item_impl.items {
            match item {
                ImplItem::Fn(fn_item) => {
                    existing_functions
                        .insert(fn_item.sig.to_token_stream().to_string(), fn_item.clone());
                }
                _ => impl_items.push(item),
            }
        }
        // Deduplicate attributes
        for attr in item_impl.attrs {
            existing_attrs.insert(attr.to_token_stream().to_string(), attr);
        }
    }

    // Add the deduplicated values to the end of the impl block (in order)
    impl_items.extend(
        existing_functions
            .values()
            .map(|fn_item| ImplItem::Fn(fn_item.clone()))
            .collect::<Vec<ImplItem>>(),
    );
    new_item_impl(
        target,
        &impl_items,
        &(existing_attrs.values().map(clone).collect()),
    )
}

/// Generates a new `impl $target` block with the provides items and attributes
// FIXME
#[allow(clippy::ptr_arg)]
pub fn new_item_impl(
    target: &str,
    impl_items: &Vec<ImplItem>,
    impl_attrs: &Vec<Attribute>,
) -> Item {
    let mut punctuated_path: Punctuated<PathSegment, Token![::]> = Punctuated::new();
    punctuated_path.push(PathSegment {
        ident: Ident::new(target, Span::call_site()),
        arguments: Default::default(),
    });

    Item::Impl(ItemImpl {
        attrs: impl_attrs.clone(),
        defaultness: None,
        unsafety: None,
        impl_token: Default::default(),
        generics: Default::default(), // TODO in https://codeberg.org/LoveIsGrief/inheriteRS/issues/15
        trait_: None,
        self_ty: Box::new(Type::Path(TypePath {
            qself: None,
            path: Path {
                leading_colon: None,
                segments: punctuated_path,
            },
        })),
        brace_token: Default::default(),
        items: impl_items.clone(),
    })
}
