pub fn is_ok<T, E>(result: &Result<T, E>) -> bool {
    result.is_ok()
}

pub fn clone<T: Clone>(any: &T) -> T {
    any.clone()
}
