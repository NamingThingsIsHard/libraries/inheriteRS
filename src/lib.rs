use proc_macro::TokenStream;

use anyhow::{anyhow, Context, Result};
use indexmap::IndexMap;
use quote::ToTokens;
use syn::{Attribute, Fields, FieldsNamed, FieldsUnnamed, Item, ItemImpl, ItemStruct};

use crate::models::{InheritAttribute, StructValue};
use crate::utils::item_impl::{get_target, merge_impl_blocks};
use crate::utils::predicates::clone;

mod models;
mod utils;

#[proc_macro]
pub fn specialisations(token_stream: TokenStream) -> TokenStream {
    let file_ast: syn::File = syn::parse(token_stream).expect("Couldn't parse TokenStream");
    TokenStream::from(
        impl_merge_macro(&file_ast)
            .expect("Couldn't generate new file")
            .to_token_stream(),
    )
}

/// Handles inheritance of structs (attributes and functions) in the file.
///
/// A new file is returned where the order of symbols and expressions is **not** guaranteed:
///   - `impl` blocks of specialised structs will come towards the end of the file
///   - functions within the `impl` blocks will also be towards the end of the block
fn impl_merge_macro(file_ast: &syn::File) -> Result<syn::File> {
    let mut new_items: Vec<Item> = vec![];
    let mut struct_cache: IndexMap<String, StructValue> = IndexMap::new();
    let mut impl_cache: IndexMap<String, ItemImpl> = IndexMap::new();
    // FIXME
    #[allow(unused_doc_comments)]
    /// `impl` blocks that have to be processed for inheritance
    let mut impl_todo: IndexMap<String, ()> = IndexMap::new();

    // First pass to collect items, build the cache, and processing list
    for item in file_ast.clone().items {
        match item {
            Item::Impl(impl_item) => {
                if impl_item.trait_.is_some() {
                    return Err(anyhow!(
                        "Cannot handle trait implementations yet. TODO in https://codeberg.org/LoveIsGrief/inheriteRS/issues/12"
                    ));
                }
                let target = get_target(&impl_item).context("impl does not have a target")?;
                impl_cache.insert(target.clone(), impl_item.clone());

                let the_struct = struct_cache.get(&target.clone()).ok_or(anyhow!(
                    "struct {} was not processed before impl block",
                    target.clone()
                ))?;
                // Process inherited implementations in another loop
                if !the_struct.parents.is_empty() {
                    impl_todo.insert(target.clone(), ());
                }
                // Add impl to output if the struct is also added to the output
                else if the_struct.output {
                    new_items.push(Item::Impl(impl_item));
                }
            }
            Item::Struct(s) => {
                let struct_value = StructValue::from(s.clone());
                let mut parents: Vec<ItemStruct> = vec![];
                for parent_name in struct_value.parents.clone() {
                    let parent_str = parent_name.as_str();
                    let parent = struct_cache
                        .get(parent_str)
                        .ok_or(anyhow!("Struct {} is unknown", parent_str))?
                        .item
                        .clone();
                    parents.push(parent);
                }

                let new_struct = merge_structs(struct_value.item.clone(), parents.as_slice())
                    .context(format!(
                        "Couldn't merge child with parents {}",
                        struct_value.parents.join(",")
                    ))?;
                struct_cache.insert(
                    struct_value.name.clone(),
                    StructValue {
                        item: new_struct.clone(),
                        output: struct_value.output,
                        parents: struct_value.parents.clone(),
                        name: struct_value.name.clone(),
                    },
                );

                if struct_value.output {
                    if !parents.is_empty() {
                        impl_todo.insert(struct_value.name.clone(), ());
                    }
                    new_items.push(Item::Struct(new_struct));
                }
            }
            _ => new_items.push(item.clone()),
        }
    }
    // Second pass
    // Handle inherited implementations
    for (name, _) in impl_todo {
        let struct_value = struct_cache.get(&name).ok_or(anyhow!(
            "Could not find struct {} for implementations inheritance",
            name
        ))?;
        if struct_value.output {
            let mut item_impls: Vec<ItemImpl> = vec![];
            for parent in struct_value.parents.clone() {
                if let Some(item_impl) = impl_cache.get(&parent) {
                    item_impls.push(item_impl.clone());
                }
            }
            let target = struct_value.name.as_str();
            if let Some(child_item_impl) = impl_cache.get(target) {
                item_impls.push(child_item_impl.clone());
            }
            if !item_impls.is_empty() {
                let new_impl = merge_impl_blocks(target, item_impls);
                new_items.push(new_impl)
            }
        }
    }
    Ok(syn::File {
        shebang: None,
        attrs: Vec::new(),
        items: new_items,
    })
}

fn merge_structs(child: ItemStruct, parents: &[ItemStruct]) -> Result<ItemStruct> {
    match parents.len() {
        0 => Ok(child),
        1 => {
            let parent = parents
                .first()
                .ok_or(anyhow!("Could not get first parent"))?;
            Ok(ItemStruct {
                attrs: clean_attrs(&child.attrs),
                vis: child.vis,
                struct_token: child.struct_token,
                ident: child.ident,
                generics: child.generics,
                fields: merge_fields(&(parent.fields), &child.fields),
                semi_token: child.semi_token,
            })
        }
        _ => {
            let merged = merge_structs(child, &parents[0..1])
                .context("Couldn't merge other parent structs")?;
            merge_structs(merged, &parents[1..])
        }
    }
}

/// Removes the #[inherit(...)] attribute from the attribute list
#[allow(clippy::ptr_arg)]
fn clean_attrs(attrs: &Vec<Attribute>) -> Vec<Attribute> {
    attrs
        .iter()
        .filter(|attr| InheritAttribute::try_from((*attr).clone()).is_err())
        .map(clone)
        .collect()
}

fn merge_fields(left: &Fields, right: &Fields) -> Fields {
    match (left, right) {
        (Fields::Unit, Fields::Unit) => panic!("Cannot merge units"),
        (Fields::Unnamed(a), Fields::Unnamed(b)) => Fields::Unnamed(FieldsUnnamed {
            paren_token: a.paren_token,
            unnamed: a.unnamed.iter().chain(b.unnamed.iter()).cloned().collect(),
        }),
        (Fields::Named(a), Fields::Named(b)) => Fields::Named(FieldsNamed {
            brace_token: a.brace_token,
            named: a.named.iter().chain(b.named.iter()).cloned().collect(),
        }),
        (_, _) => panic!("Field should be of the same type!"),
    }
}

#[cfg(test)]
mod test {
    use crate::impl_merge_macro;

    #[test]
    fn test_inherit_with_attrs() {
        let file = syn::parse_str(
            "
            struct ABC {
                attr1: u8,
            }
            #[inherit(ABC)]
            #[derive(Serialize, Deserialize)]
            #[some_macro()]
            struct CDE {
                attr2: u8,
            }
            #[inherit(CDE)]
            struct EFG {
                attr3: u8,
            }
        ",
        )
        .unwrap();
        let out = prettyplease::unparse(&impl_merge_macro(&file).unwrap());
        assert_eq!(
            out,
            "\
struct ABC {
    attr1: u8,
}
#[derive(Serialize, Deserialize)]
#[some_macro()]
struct CDE {
    attr1: u8,
    attr2: u8,
}
struct EFG {
    attr1: u8,
    attr2: u8,
    attr3: u8,
}
"
        );
    }

    #[test]
    /// Ensure the order of attributes in specialised structs is by order of inheritance, nothing else
    fn test_inherited_attr_order() {
        let file = syn::parse_str(
            "
            struct ABC {
                attr1: u8,
                attr2: u8,
            }
            #[inherit(ABC)]
            struct CDE {
                attr3: u8,
                attr0: u8
            }
            #[inherit(CDE)]
            struct EFG {
                z_attr: u8,
                attr5: u8,
            }
        ",
        )
        .unwrap();
        // attr0 and z_attr are to ensure the attributes are alphabetically ordered
        let out = prettyplease::unparse(&impl_merge_macro(&file).unwrap());
        assert_eq!(
            out,
            "\
struct ABC {
    attr1: u8,
    attr2: u8,
}
struct CDE {
    attr1: u8,
    attr2: u8,
    attr3: u8,
    attr0: u8,
}
struct EFG {
    attr1: u8,
    attr2: u8,
    attr3: u8,
    attr0: u8,
    z_attr: u8,
    attr5: u8,
}
"
        );
    }

    #[test]
    /// Ensure a base class marked with #[base(output=false)] is ignored in the output
    fn test_inherited_ignored_output() {
        let file = syn::parse_str(
            "
            #[base(output=false)]
            struct ABC {
                attr1: u8,
                attr2: u8,
            }
            #[inherit(ABC)]
            struct CDE {
                attr3: u8,
                attr0: u8
            }
        ",
        )
        .unwrap();
        let out = prettyplease::unparse(&impl_merge_macro(&file).unwrap());
        assert_eq!(
            out,
            "\
struct CDE {
    attr1: u8,
    attr2: u8,
    attr3: u8,
    attr0: u8,
}
"
        );
    }

    #[test]
    /// Simple `impl $target` blocks should be inherited and merged with the children
    fn test_non_trait_functions() {
        let file = syn::parse_str(
            "
            struct ABC {}
            impl ABC {
                fn some_function() -> u8 {
                    99
                }
            }
            struct CDE {}
            impl CDE {
                fn another_function() -> u8 {
                    66
                }
            }
            #[inherit(ABC)]
            #[inherit(CDE)]
            struct EFG {}
        ",
        )
        .unwrap();
        let out = prettyplease::unparse(&impl_merge_macro(&file).unwrap());
        assert_eq!(
            out,
            "\
struct ABC {}
impl ABC {
    fn some_function() -> u8 {
        99
    }
}
struct CDE {}
impl CDE {
    fn another_function() -> u8 {
        66
    }
}
struct EFG {}
impl EFG {
    fn some_function() -> u8 {
        99
    }
    fn another_function() -> u8 {
        66
    }
}
"
        );
    }

    #[test]
    /// Methods implemented in the parent should be overridable by the child
    fn test_override_non_trait_methods() {
        let file = syn::parse_str(
            "
            struct Parent {}
            impl Parent {
              fn method () { println!(\"do something in parent\"); }
              fn another_method() { println!(\"do something else from parent\"); }
            }

            #[inherit(Parent)]
            struct Child {}
            impl Child {
              fn method () { println!(\"do something in child\"); }
            }
        ",
        )
        .unwrap();
        let out = prettyplease::unparse(&impl_merge_macro(&file).unwrap());
        assert_eq!(
            out,
            "\
struct Parent {}
impl Parent {
    fn method() {
        println!(\"do something in parent\");
    }
    fn another_method() {
        println!(\"do something else from parent\");
    }
}
struct Child {}
impl Child {
    fn method() {
        println!(\"do something in child\");
    }
    fn another_method() {
        println!(\"do something else from parent\");
    }
}
"
        );
    }

    #[test]
    /// Ensure an impl block of a base class marked with #[base(output=false)] is ignored in the output
    fn test_ignore_parent_non_trait_function() {
        let file = syn::parse_str(
            "
            #[base(output=false)]
            struct ABC {}
            impl ABC {
                fn some_function() -> u8 {
                    99
                }
            }
            #[inherit(ABC)]
            struct CDE {}
        ",
        )
        .unwrap();
        let out = prettyplease::unparse(&impl_merge_macro(&file).unwrap());
        assert_eq!(
            out,
            "\
struct CDE {}
impl CDE {
    fn some_function() -> u8 {
        99
    }
}
"
        );
    }
}
