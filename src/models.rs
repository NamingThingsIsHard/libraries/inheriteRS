use syn::{Attribute, ItemStruct, Meta};

use crate::utils::item_structs::{extract_base_attr, extract_parent_names};

/// Holds an ItemStruct and related information necessary to inheriteRS
pub struct StructValue {
    pub item: ItemStruct,
    pub output: bool,
    pub parents: Vec<String>,
    pub name: String,
}

impl From<ItemStruct> for StructValue {
    fn from(item: ItemStruct) -> Self {
        StructValue {
            item: item.clone(),
            name: item.ident.to_string(),
            output: match extract_base_attr(&item) {
                None => true,
                Some(attr) => attr.output,
            },
            parents: extract_parent_names(&item),
        }
    }
}

/// Information about an `#[inherit]` attribute
pub struct InheritAttribute {
    /// The parent or base struct (not class) of the struct this was tagged with
    pub base: String,
}

impl TryFrom<Attribute> for InheritAttribute {
    type Error = &'static str;

    fn try_from(attr: Attribute) -> Result<Self, Self::Error> {
        let meta_list = match attr.meta.clone() {
            Meta::List(l) => l,
            _ => return Err("No metalist"),
        };
        let ident = meta_list.path.get_ident().expect("No identifier");
        if ident != "inherit" {
            return Err("Not an inherit attribute");
        }

        let base = meta_list.tokens.to_string();
        if !base.is_empty() {
            Ok(InheritAttribute { base })
        } else {
            Err("No base struct provided for inherit attribute")
        }
    }
}

/// The `#[base]` attribute which is used to configure behavior related to a base (parent) struct
#[derive(Copy, Clone)]
pub struct BaseAttribute {
    /// Whether the tagged struct should be in the output of the algorithm
    pub output: bool,
}

impl TryFrom<Attribute> for BaseAttribute {
    type Error = &'static str;

    fn try_from(attr: Attribute) -> Result<Self, Self::Error> {
        let meta_list = match attr.meta.clone() {
            Meta::List(l) => l,
            _ => return Err("No metalist"),
        };
        let ident = meta_list.path.get_ident().expect("No identifier");
        if ident != "base" {
            return Err("Not a base attribute");
        }

        let target = meta_list.tokens.to_string();
        let mut output = true;
        for var_str in target.split(",") {
            // FIXME
            #[allow(clippy::expect_fun_call)]
            let (key_orig, value_orig) = var_str
                .split_once("=")
                .expect(format!("Cannot split '{}'", var_str).as_str());
            let (key, value) = (key_orig.trim(), value_orig.trim());
            if key == "output" {
                output = value == "true"
            }
        }
        Ok(BaseAttribute { output })
    }
}

#[cfg(test)]
mod test {
    use syn::{File, Item, ItemStruct};

    use crate::models::BaseAttribute;

    #[test]
    fn test_base_attribute_unknown_params() {
        let file: File = syn::parse_str(
            "
            #[base(unknown=abc, something=true)]
            struct ABC {
                a: u8
            }
        ",
        )
        .expect("Couldn't parse string");
        // FIXME
        #[allow(clippy::unwrap_used)]
        let structs: Vec<&ItemStruct> = file
            .items
            .iter()
            .map(|item| match item {
                Item::Struct(s) => Some(s),
                _ => None,
            })
            .filter(|x| x.is_some())
            .map(|x| x.unwrap())
            .collect();
        let _struct = structs.first().expect("No structs found");
        let base = BaseAttribute::try_from(
            _struct
                .attrs
                .first()
                .cloned()
                .expect("Must have one attribute"),
        )
        .expect("No attribute could be parsed");
        assert!(base.output)
    }
}
