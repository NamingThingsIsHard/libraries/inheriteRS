A proc-macro to enable inheritance in rust without runtime performance implications.

# Examples

## Inheriting attributes

```rust
use inheriters::specialisations;

specialisations!(
    struct Parent {
        attr1: u8,
    }
    #[inherit(Child)]
    struct Child {
        attr2: u8,
    }
);
```

will result in

```rust
struct Parent {
    attr1: u8,
}
struct Child {
    attr1: u8,
    attr2: u8,
}
```

## Inheriting and overriding functions

By default, non-trait struct implementations (aka `impl StructName` **not** `impl Trait for StructName`)
will be inherited by descendants.
Those functions can also be overridden.

```rust
use inheriters::specialisations;

specialisations!(
    struct Parent {
        attr1: u8,
    }
    impl Parent {
        fn from_parent(self) -> u8 { 8 }
        fn overridden(self) -> u8 { self.attr1 }
    }
    
    #[inherit(Child)]
    struct Child {
        attr2: u8,
    }
    impl Child {
        fn overridden(self) -> u8 { self.attr2 }
    }
);
```

results in 

```rust
struct Parent {
    attr1: u8,
}
impl Parent {
    fn from_parent(self) -> u8 { 8 }
    fn overridden(self) -> u8 { self.attr1 }
}


struct Child {
    attr1: u8, // new
    attr2: u8,
}
impl Child {
    fn from_parent(self) -> u8 { 8 } // new
    fn overridden(self) -> u8 { self.attr2 }
}
```
